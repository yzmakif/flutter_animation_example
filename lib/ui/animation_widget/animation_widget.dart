import 'package:flutter/material.dart';

class AnimationWidgets extends StatefulWidget {
  AnimationWidgets({Key key}) : super(key: key);

  @override
  _AnimationWidgetsState createState() => _AnimationWidgetsState();
}

class _AnimationWidgetsState extends State<AnimationWidgets> {
  Color _color = Colors.pink;
  var _width = 200.0;
  var _heigth = 200.0;
  var _opacity = 1.0;
  var _ilkCocukAktif = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Animation Widgets"),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              AnimatedCrossFade(
                firstChild: Image.network("https://cdn.figurex.net/wp-content/uploads/2017/02/asdfghjk.jpg"),
                secondChild: Image.network("https://frigbo.com/sites/default/files/post/images/black-clover.jpeg"),
                crossFadeState: _ilkCocukAktif ? CrossFadeState.showFirst : CrossFadeState.showSecond,
                duration: Duration(milliseconds: 750),
              ),
              RaisedButton(
                onPressed: () => _crossFadeAnimasyon(),
                color: Colors.blue,
                child: Text("Cross Fade"),
              ),
              AnimatedContainer(
                duration: Duration(milliseconds: 750),
                color: _color,
                height: _heigth,
                width: _width,
              ),
              RaisedButton(
                onPressed: () => _animatedContainerAnimasyon(),
                color: Colors.blue,
                child: Text("Animated Container"),
              ),
              AnimatedOpacity(
                opacity: _opacity,
                duration: Duration(seconds: 1),
                child: FlutterLogo(
                  size: 100,
                ),
              ),
              RaisedButton(
                onPressed: () => _opacityAnimasyon(),
                color: Colors.blue,
                child: Text("Animated Opacity"),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _animatedContainerAnimasyon() {
    setState(() {
      _color = _color == Colors.pink ? Colors.yellow : Colors.pink;
      _width = _width == 200.0 ? 300.0 : 200.0;
      _heigth = _heigth == 200.0 ? 300.0 : 200.0;
    });
  }

  void _crossFadeAnimasyon() {
    setState(() {
      _ilkCocukAktif = _ilkCocukAktif ? false : true;
    });
  }

  void _opacityAnimasyon() {
    setState(() {
      _opacity = _opacity == 1.0 ? 0.0 : 1.0;
    });
  }
}
