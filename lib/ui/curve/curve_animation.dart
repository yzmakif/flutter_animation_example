import 'package:flutter/material.dart';

class CurveAnimation extends StatefulWidget {
  CurveAnimation({Key key}) : super(key: key);

  @override
  _CurveAnimationState createState() => _CurveAnimationState();
}

class _CurveAnimationState extends State<CurveAnimation> with SingleTickerProviderStateMixin {
  int _counter = 0;
  AnimationController controller;
  Animation animation;
  Animation animation2;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(vsync: this, duration: Duration(seconds: 2));

    controller.addListener(() {
      setState(() {
        //print(controller.value.toString());
      });
    });
    animation = CurvedAnimation(parent: controller, curve: Curves.easeInOutExpo);
    animation2 = AlignmentTween(begin: Alignment.topLeft, end: Alignment.bottomRight).animate(animation);

    controller.forward();
    controller.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        controller.reverse().orCancel;
      } else if (status == AnimationStatus.dismissed) {
        controller.forward().orCancel;
      }
      //print("Durum : " + status.toString());
    });
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Curve Animation"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
              style: TextStyle(fontSize: animation.value * 18),
            ),
            Container(
              height: 200,
              alignment: animation2.value,
              child: Text(
                '$_counter',
                style: Theme.of(context).textTheme.headline4,
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}
