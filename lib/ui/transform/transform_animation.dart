import 'package:flutter/material.dart';

class TransfornAnimation extends StatefulWidget {
  TransfornAnimation({Key key}) : super(key: key);

  @override
  _TransfornAnimationState createState() => _TransfornAnimationState();
}

class _TransfornAnimationState extends State<TransfornAnimation> {
  var _sliderValue = 0.0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Transform Animation"),
      ),
      body: Column(
        children: <Widget>[
          getSlider(),
          SizedBox(
            height: 10,
          ),
          getRotate(),
          SizedBox(
            height: 75,
          ),
          getScale(),
          SizedBox(
            height: 75,
          ),
          getTranslate(),
        ],
      ),
    );
  }

  Slider getSlider() {
    return Slider(
        min: 0,
        max: 100,
        value: _sliderValue,
        onChanged: (newValue) {
          setState(() {
            _sliderValue = newValue;
          });
        });
  }

  Container getRotate() {
    return Container(
      child: Transform.rotate(
        angle: _sliderValue,
        origin: Offset(_sliderValue, 0),
        child: Container(
          width: 100,
          height: 100,
          color: Colors.blue,
        ),
      ),
    );
  }

  Container getScale() {
    return Container(
      child: Transform.scale(
        scale: _sliderValue == 0 ? 1 : _sliderValue / 50,
        child: Container(
          width: 100,
          height: 100,
          color: Colors.blue,
        ),
      ),
    );
  }

  getTranslate() {
    return Container(
      child: Transform.translate(
        offset: Offset(_sliderValue, _sliderValue),
        child: Container(
          width: 100,
          height: 100,
          color: Colors.blue,
        ),
      ),
    );
  }
}
