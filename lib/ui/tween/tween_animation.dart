import 'package:flutter/material.dart';

class TweenAnimation extends StatefulWidget {
  TweenAnimation({Key key}) : super(key: key);

  @override
  _TweenAnimationState createState() => _TweenAnimationState();
}

class _TweenAnimationState extends State<TweenAnimation> with SingleTickerProviderStateMixin {
  int _counter = 0;
  AnimationController controller;
  Animation<Color> animation;
  Animation<Alignment> animation2;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(vsync: this, duration: Duration(seconds: 1));

    controller.addListener(() {
      setState(() {
        //print(controller.value.toString());
        //print(animation.value.toString());
      });
    });

    animation = ColorTween(begin: Colors.blue, end: Colors.yellow).animate(controller);
    animation2 = AlignmentTween(begin: Alignment.topLeft, end: Alignment.bottomRight).animate(controller);

    controller.reverse();
    controller.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        controller.reverse().orCancel;
      } else if (status == AnimationStatus.dismissed) {
        controller.forward().orCancel;
      }
      //print("Durum : " + status.toString());
    });
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: animation.value,
      appBar: AppBar(
        title: Text("Tween Animation"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            Container(
              height: 200,
              alignment: animation2.value,
              child: Text(
                '$_counter',
                style: Theme.of(context).textTheme.headline4,
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}
