import 'package:flutter/material.dart';
import 'package:flutter_animasyonlar/ui/animation_widget/animation_widget.dart';
import 'package:flutter_animasyonlar/ui/curve/curve_animation.dart';
import 'package:flutter_animasyonlar/ui/hero/hero_animation.dart';
import 'package:flutter_animasyonlar/ui/staggered/staggered_animation.dart';
import 'package:flutter_animasyonlar/ui/tween/tween_animation.dart';

import 'controller/animation_controller_1.dart';
import 'transform/transform_animation.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            //Hero Animation
            Card(
              elevation: 2,
              margin: EdgeInsets.all(10),
              child: ListTile(
                title: Text(
                  "Hero Animation",
                  style: TextStyle(fontWeight: FontWeight.w500),
                ),
                onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => HeroAnimation())),
              ),
            ),
            //Animation Controller
            Card(
              elevation: 2,
              margin: EdgeInsets.all(10),
              child: ListTile(
                title: Text(
                  "Animation Controller",
                  style: TextStyle(fontWeight: FontWeight.w500),
                ),
                onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => AnimationController1())),
              ),
            ),
            //Tween Animation
            Card(
              elevation: 2,
              margin: EdgeInsets.all(10),
              child: ListTile(
                title: Text(
                  "Tween Animation",
                  style: TextStyle(fontWeight: FontWeight.w500),
                ),
                onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => TweenAnimation())),
              ),
            ),
            //Curve Animation
            Card(
              elevation: 2,
              margin: EdgeInsets.all(10),
              child: ListTile(
                title: Text(
                  "Curve Animation",
                  style: TextStyle(fontWeight: FontWeight.w500),
                ),
                onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => CurveAnimation())),
              ),
            ),
            //Staggered Animation
            Card(
              elevation: 2,
              margin: EdgeInsets.all(10),
              child: ListTile(
                title: Text(
                  "Staggered Animation",
                  style: TextStyle(fontWeight: FontWeight.w500),
                ),
                onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => StaggeredAnimation())),
              ),
            ),
            //Animation Widgets
            Card(
              elevation: 2,
              margin: EdgeInsets.all(10),
              child: ListTile(
                title: Text(
                  "Animation Widgets",
                  style: TextStyle(fontWeight: FontWeight.w500),
                ),
                onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => AnimationWidgets())),
              ),
            ),
            //Transform Animation
            Card(
              elevation: 2,
              margin: EdgeInsets.all(10),
              child: ListTile(
                title: Text(
                  "Transform Animation",
                  style: TextStyle(fontWeight: FontWeight.w500),
                ),
                onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => TransfornAnimation())),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
