import 'package:flutter/material.dart';

class HeroAnimation extends StatelessWidget {
  const HeroAnimation({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Hero Animation"),
      ),
      body: Center(
        child: InkWell(
          onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => Hero2())),
          child: Hero(
            tag: "hero",
            child: FlutterLogo(
              size: 50,
            ),
          ),
        ),
      ),
    );
  }
}

class Hero2 extends StatelessWidget {
  const Hero2({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Hero Animation"),
      ),
      body: Center(
        child: Hero(
          tag: "hero",
          child: FlutterLogo(
            size: 250,
          ),
        ),
      ),
    );
  }
}
