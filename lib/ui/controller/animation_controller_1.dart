import 'package:flutter/material.dart';

class AnimationController1 extends StatefulWidget {
  AnimationController1({Key key}) : super(key: key);

  @override
  _AnimationController1State createState() => _AnimationController1State();
}

class _AnimationController1State extends State<AnimationController1> with SingleTickerProviderStateMixin {
  int _counter = 0;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(vsync: this, duration: Duration(seconds: 1), lowerBound: 20, upperBound: 50);

    controller.addListener(() {
      setState(() {
        print(controller.value.toString());
      });
    });

    controller.forward(from: 20);

    controller.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        controller.reverse().orCancel;
      } else if (status == AnimationStatus.dismissed) {
        controller.forward().orCancel;
      }
      print("Durum : " + status.toString());
    });
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Animation Controller"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4.copyWith(fontSize: controller.value),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}
